# TTN-influxdb

Retrieve data from The Things Network to store in a influxDB for visualization in Grafana.

## Services

TTN -> Telegraf -> InfluxDB -> Grafana


## TTN
Quote from https://www.thethingsnetwork.org/docs/applications/mqtt/ :

 
>  The Things Network uses MQTT to publish device activations and messages, but also allows you to publish a message for a specific device in response.

In MQTT argo, the TTN is a Broker, so we could simply start getting our data by subscribing to published topics. Let's do it with the [mosquito_sub](https://mosquitto.org/man/mosquitto_sub-1.html) client:

First get Application ID and Access key from TTN console:

Applications -> microchip-motes -> Overview

![Authentication keys:](/resources/accessKey.png)

So we use the **Application ID** as the user, and the **Access Keys** as the password to subscribe to all devices registered in this application. Host to subscribe is the last part of the **ttn-handler** (us-west)

From command line run:

`mosquitto_sub -h us-west.thethings.network -d -t '+/devices/+/up'  -u microchip-motes -P ttn-account-v2.this_is_the_password`



### Integrations
TTN offers some SDK to interface with its api, in the future we could use the python SDK to deeper integration with the platform. See this link:

https://www.thethingsnetwork.org/docs/applications/python/

But in this stage, we decided to get data from TTN without using its API, given that MQTT clients are vastly available. For retrieving the data we choose Telegraf.

#### Microchip-motes payload format

As a use case of integration, we describe how the sensors data are encoding in the LoRaWAN packet's payload. The bytes transmitted are coding in ASCII charcaters and the order is: <pre>[port][' '][light][' '][temperature]</pre>

| bytes | description |
| ------ | ------ |
| 3 | port number |
| 1 | space character | 
| 1 to 5 | light (the firmware reduces the bytes to transmit, it deletes the most significant zeros therefore the amount bytes could change) | 
| 1 | space character (this byte must be used to determinate the size of light sensor bytes) | 
| 3 | temperature |


Following this format, the TTN provides a custo payload format "parser", as an example, we wrote this implementation of the above format, that can be found in:

Applications -> microchip-motes -> Payload Formats


## Retrieving the data
In order to save TTN data to our database, we use the Telegraf agent. This agent gets data from the TTN using its builtin mqtt client and store data into the InfluxDB.


Into the telegraf configuration file (/etc/telegraf/telegraf.conf) add the following:

```
[[inputs.mqtt_consumer]]
    servers = ["tcp://us-west.thethings.network:1883"]
    qos = 0
    connection_timeout = "30s"
    topics = [ "+/devices/+/up" ]
    client_id = ""
    username = "microchip-motes"
    password = "ttn-account-v2.this_is_a_hidden_password"
    data_format = "json"
    json_string_fields = ["dev_id"]

```



